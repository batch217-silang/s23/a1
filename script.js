// console.log("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	attack: 50,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log('Pikachu! I choose you!');
	}
}
console.log(trainer);
console.log("Result from DOT notation: ");
console.log(trainer.name);
console.log("Result from SQUARE BRACKET notation: ");
console.log(trainer['pokemon']);
console.log('Result of talk method');
trainer.talk();


// CONSTRUCTOR 
let Pikachu = {
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function () {
		console.log("pokemon fainted");
	}
}
console.log(Pikachu);

let Geodude = {
	name: "Geodude",
	level: 8,
	health: 16,
	attack: 8,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function () {
		console.log("pokemon fainted");
	}
}
console.log(Geodude)


let Mewtwo = {
	name: "Mewtwo",
	level: 100,
	health: 200,
	attack: 100,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function () {
		console.log("pokemon fainted");
	}
}
console.log(Mewtwo);

function Pokemon(name, level, health){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + " health is now reduced to 16");
	},
	this.faint = function(){
		console.log(this.name + "fainted");
	}
	
}

let geodude = new Pokemon("geodude");
let pikachu = new Pokemon("geodude");
geodude.tackle(pikachu);

let mewtwo = new Pokemon("mewtwo");
mewtwo.tackle(geodude);







